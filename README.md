# Backscattering Using PIC12

This Firmware tries to acomplish full RFID Tag emulation using a PIC32LF1552 MCU without any additional RF componant

## Objectives

* Verify the asomptions described in (paper ref)
* developp improvements
* Implement reception without additional RF componants
* Implement full RFID protocol 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* MPLABX Toolchain

### Deployment

#### Using MPLABX IDE
TODO
#### Using MPLABX commandline toolchain (as in pipelines)
TODO
## Project Infos

### General

TODO

### Project Structure

```
+-Backscattering_pic.X: // MPLABX project
| +-nbproject:
| | +-configurations.xml
| | +-project.xml
| +-Makefile
| +-main.c
+-LICENCE
+-README
```

## Built With

* [MPLABX](https://www.microchip.com/mplab/mplab-x-ide) - Microchip IDE
* [XC8](https://www.microchip.com/en-us/development-tools-tools-and-software/mplab-xc-compilers) - 8bit PIC Compiler
* [docker-xc8](https://hub.docker.com/r/ukaiser/docker-xc8/dockerfile) - A docker file compiling MPLAB X Toolchain


## Authors

* **Anton Dumas** - *Initial work* - [Gitlab](https://gitlab.com/AntonD_PI15) - [anton.dumas@pi.esisar.grenoble-inp.fr](anton.dumas@pi.esisar.grenoble-inp.fr)

## License

BSD 3 closes, see [LICENCE](./LICENCE) file
