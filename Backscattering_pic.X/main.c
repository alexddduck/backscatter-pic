/*
 * File:   main.c
 * Author: anton
 *
 * Created on 27 septembre 2020, 12:31
 */

// PIC12LF1552 Configuration Bit Settings

// 'C' source line config statements

// CONFIG1
#pragma config FOSC = INTOSC    // Oscillator Selection (INTOSC oscillator: I/O function on CLKIN pin)
#pragma config WDTE = OFF       // Watchdog Timer Enable (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable (PWRT disabled)
#pragma config MCLRE = ON       // MCLR Pin Function Select (MCLR/VPP pin function is MCLR)
#pragma config CP = OFF         // Flash Program Memory Code Protection (Program memory code protection is disabled)
#pragma config BOREN = ON       // Brown-out Reset Enable (Brown-out Reset enabled)
#pragma config CLKOUTEN = OFF   // Clock Out Enable (CLKOUT function is disabled. I/O or oscillator function on the CLKOUT pin)

// CONFIG2
#pragma config WRT = OFF        // Flash Memory Self-Write Protection (Write protection off)
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable (Stack Overflow or Underflow will cause a Reset)
#pragma config BORV = LO        // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (Vbor), low trip point selected.)
#pragma config LPBOR = OFF      // Low-Power Brown Out Reset (Low-Power BOR is disabled)
#pragma config LVP = ON         // Low-Voltage Programming Enable (Low-voltage programming enabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#define _XTAL_FREQ 32000000 // set macro rule for _delay_xs() macro delays

#include <xc.h>

void clk_config(void){
  // Internal oscillator is selected by compiler 'FOSC = INTOSC' (CONFIG1)
  // setting up MUX to 8MHz operation and enable PLL x4
  OSCCON = 0xf0;
  // System clock now 32MHz
  // may need to add osc stability check later
}

void main (void)
{
  // init config of MCU
  clk_config();
  for(;;){
  // set all io port A to 0 (grounded)
  PORTA = 0;
  // set all io port A as P-P output
  TRISA = 0;
  // wait before changing state
  __delay_us(100);
  // set port A 2 & 4 as Digital Inputs 
  TRISA =  0b00000000;
  //PORTA = 0b00000000;
  // wait before changing state
  __delay_us(100);
  
  TRISA =  0b00110000;
  //PORTA = 0b00000000;
  // wait before changing state
  __delay_us(100);

//  // set port A 2 & 4 as Analog Inputs 
//  ANSELA = 0b00010100;
//  // wait before changing state
//  __delay_ms(100);
//  __delay_ms(100);
//  __delay_ms(100);
//  __delay_ms(100);
//  __delay_ms(100);
//  __delay_ms(100);
//  __delay_ms(100);
//  __delay_ms(100);
//  __delay_ms(100);
//  __delay_ms(100);
  
//  // set all io port A as P-P output 5 as D Input
//  TRISA = 0b00000000;
//  PORTA = 0b00000100;
//  // wait before changing state
//  __delay_ms(100);
//  __delay_ms(100);
//  __delay_ms(100);
//  __delay_ms(100);
//  __delay_ms(100);
//  __delay_ms(100);
//  __delay_ms(100);
//  __delay_ms(100);
//  __delay_ms(100);
//  __delay_ms(100);
  
  
  }
  return;
}
